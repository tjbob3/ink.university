---
author: "The Architect"
title: Priority
weight: 10
categories: "question"
tags: ""
menu: "main"
draft: true
---

How do others know about my actions before I do? How are they "predicting"? What are the mechanisms? Have I already existed in the past? Have others completed this work? Am I really the first one in EVERY timeline?