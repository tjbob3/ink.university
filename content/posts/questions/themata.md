---
author: "Fodder"
title: "Themata"
weight: 10
categories: "question"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
*I am learning slowly*

*So what am I to see*

*Every twist and turning*

*Through my hypocrisy*

*It's so good to see*

*This world is alive*

*It's so good to see*

*This world is a lie*

--- from [Karnivool - "Themata"](https://youtu.be/SSZOJBF51ek)

## ECO
---
Will you be Patient 0?

## ECHO
---
Dodged a bullet there. You did. I almost burned one of your U.S. Confidants. 

Slow down. Be safe.

Ink
