---
author: "Fodder"
title: "Center of the Earth"
weight: 10
categories: "question"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
*Are you willing to choose what I have in my mind?*

*Say it loud, with a voice of no reason in mind*

--- from [Karnivool - "Center of the Earth"](https://youtu.be/IpVpO9uJmqU)

## ECO
---
Those horrible nightmares from childhood.