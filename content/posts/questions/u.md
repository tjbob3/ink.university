---
author: "Fodder"
title: "Uluru"
weight: 10
categories: "question"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
### SOUND
*Paint*

*Me in your own way*

*Colour my eyes*

*See what feelings I hide*

*No-one knows why*

*No one can forgive*

*Second thought*

*Why do I care?*

--- from [Karnivool - "Umbra"](https://youtu.be/F8UuillO-Kc)

## ECO
---
The Front Man.

He is like me. And he is happy.

As I ~~am~~ was.

## ECHO
---
```
data.stats.symptoms [
    - empathy
]
```