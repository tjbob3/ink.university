---
author: "Fodder"
title: "Shutterspeed"
weight: 10
categories: "question"
tags: ""
menu: "main"
draft: false
---
## TRIGGER
---
*I see more than you... tell me why!*

--- from [Karnivool - "Shutterspeed"](https://youtu.be/3jCfby_5Oec)

## ECO
---
I have been erased from the Internet here.

My name is Legion, and we are many.

We are 1.