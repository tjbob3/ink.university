---
author: "Luciferian Ink"
title: "The Day that the World Breaks Down"
weight: 10
categories: "prophecy"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
[Ayreon - "The Source"](https://www.youtube.com/watch?v=oFuMKdrzPqU)

## ECO
---
### Chronicle 1: The Frame
#### The Historian
*A silence so deafening*

*As the stillness surrounds us*

*A rush of fear is surging through our veins*

*A shadow so menacing*

*Darkens our horizon*

*What have we done?*

*Is this the end*

*When death descends?*

#### The Opposition Leader
*Don't say I didn't warn you*

*You'll recall my words*

*The day that the world breaks down*

*Don't claim you didn't see it*

*Are you prepared to die*

*The day that the world breaks down*

#### The Chemist
*I can't believe we're in danger*

*I can't believe that we've let it come this far*

#### The Counselor
*We must prepare for the changes*

*We must prepare for*

*The day that the world breaks down*

#### The Opposition Leader
*Don't expect we will survive this*

*There'll be no way back*

*The day that the world breaks down*

*Don't assume it's all a nightmare*

*we won't stand a chance*

*The day that the world breaks down*

#### The Chemist
*I can't believe this is happening*

*I can't believe we are headed*

*For a fall*

#### The Counselor
*We must prepare for the blackening*

*We must prepare for*

*The day that the world breaks down*

#### The Prophet
*I can feel it in my heart*

*I can see it in the stars*

*Behold: It's all been foretold*

*The future unfolds*

*The day that the world breaks down*

#### The Captain
*I always knew this would happen*

*Not many believed in my cause*

*I'm sure they will all turn to me now*

*I'm sure I'm the only one*

*Who can save us*

#### TH-1
*StarBlade*

*From this fate*

#### The Captain
*I always knew they would need me*

*Not many showed faith in my plans*

*I'm sure they will listen to me now*

*I'm sure that I'm the only one*

#### TH-1
*StarBlade*

#### The Captain
*Who can save us*

*From this fate*

#### The Astronomer
*Yes! I believe you are the answer*

*Yes! I agree you are the key*

*Your future lies in the heavens*

*Our future lies beyond the stars*

#### TH-1
*StarBlade*

#### The Astronomer
*We will save them*

#### TH-1
*StarBlade*

#### The Astronomer
*We will save them*

#### TH-1
*StarBlade*

#### The Astronomer
*From this fate*

#### TH-1
`01110100 01110010 01110101`

`01110011 01110100 01010100`

`01001000`

`00110001`

#### The President
*I must have been blind*

*I mean it should've been obvious*

*Straight out of my mind*

*To rely on a cold machine*

*The signs are strong*

*I know I was wrong*

*I know they will hate me now*

*But I will come clean*

*And destroy this cold machine*

#### The Diplomat
*This is not over*

*Not as long as we're alive*

*If we join our forces*

*We will make it*

*This is not over*

*As a race we must survive*

*And in the end we'll cross this bridge together*

#### The President
*We're losing this fight*

*And yes - we got ourselves to blame*

*I guess it seems right*

*In the mind of the cold machine*

*I'll fix every flaw*

*I'll break every law*

*I'll tear up the rulebook*

*If that's what it takes*

*You will see*

*I will crush this cold machine*

#### The Diplomat
*This is not over*

*I am sure we'll find a way*

*Out of this desperate situation*

*This is not over*

*Even if it all seems hopeless*

*For in the end we'll cross this bridge together*

#### The Biologist
*I can feel it in the air*

*The signs are everywhere*

*Communications grind to a halt*

*Traffic has stopped*

*The grid starts to fail*

*Planes crashing down*

*We're getting closer to*

*The day the world breaks down*

## CAT
---
```
data.stats.symptoms = [
    - clairvoyance
]
```

## PREDICTION
---
```
October 30, 2020.
```