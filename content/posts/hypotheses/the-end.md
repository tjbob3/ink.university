---
author: "The Architect"
title: The End
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## ECO
---
The death of a consciousness results in the bottom of [Prism](/docs/scenes/prism) leaving behind a UFO-like artifact.

Typically, this artifact is buried deep underground - because the back of Prism is typically in the center of the Earth.