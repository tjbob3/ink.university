---
author: "The Architect"
title: Sound
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## ECO
---
Sound was the first form of [Pillar](/docs/pillars) in our universe.

Sound is more important than sight. Sound gave shape to our universe far before sight did.