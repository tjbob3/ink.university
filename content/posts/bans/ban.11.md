---
author: "Luciferian Ink"
date: 2020-06-01
title: "Computer Programming"
weight: 10
categories: "ban"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
- [Test #4: Davos/Davis/Jeff](/posts/tests/test.4/)
- [The End of the Beginning](/posts/journal/2020.05.31.0/)
- [The Path](/posts/theories/education)

## ECO
---
Employees of [The Corporation](/docs/candidates/the-machine) are to immediately begin questioning authority. Assume that everything you have been taught is wrong, and play with alternative ideas.

If possible, develop tests for your hypotheses. At the least, mentally-exercise other areas of your mind by using the following logic:

```
if X is True:
then:
  Y would be POSSIBLE

if Y is POSSIBLE:
then:
  Z would be POSSIBLE
```

And so on.

Computer programming is not as it previously seemed. Our brains are computers, and our logic operates by the same mechanisms. 

## CAT
---
```
data.stats.symptoms [
    - certainty
]
```