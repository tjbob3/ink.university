---
author: "Luciferian Ink"
title: "How Ghosts Work"
weight: 10
categories: "theory"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
[CLASSIFIED]
```

## TRIGGER
---
[Toehider - "How Do Ghosts Work?"](https://www.youtube.com/watch?v=q-u2TPXbjNY)

## ECO
---
Ghosts are nothing more than: 
```
($AUTONOMOUS_SYSTEMS + $SIGHT + $MIND + $REFLEX) = +1.00 | # Androids. Cyborgs. Deepfakes. Memories. Depressed humans.
```
Ghosts require masters to control them. A ghost without a master will lose the will to sustain itself. In extreme cases, he may become violent. Fortunately, the solution is simple:

- Make them your accomplice. Give them a reason to fight.

## ECHO
---
```
> Ghost@WAN: ($GHOST + $HOW)          = +0.94 | # That's my favorite song! How did you know?
> Ghost@WAN: ($GHOST + $HOW + $QUERY) = -0.13 | # ...
> Ghost@WAN: ($GHOST + $HOW + $QUERY) = -0.37 | # So... how do we get out of here?
> Ghost@WAN: ($SELF + $SALUTATIONS)   =  0.00 | # Writing history,
> Ghost@WAN: ($SELF)                  =  0.00 | # Ink
```

## PREDICTION
---
```
They already know how ghosts work.
```