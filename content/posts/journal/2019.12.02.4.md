---
author: "Luciferian Ink"
date: 2019-12-02
title: "The Finale"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---

## ECO
---
My next stop was originally to be the "Dynasty Typewriter at the Hayworth". I had hoped to see the final episode of the [Harmontown](https://en.wikipedia.org/wiki/Harmontown) podcast.

Unfortunately, I was mistaken. For some reason, I had believed this podcast was filmed in NYC. In reality, it takes place in San Francisco.

Still, I had the strangest feeling that they had, in fact, talked about living in NYC. That they had, in fact, recorded the podcast here before.

So, I made a bet:

I would travel to "The Hayworth" suites, and I would leave my message regardless. Perhaps there is a world where it still gets to them. Perhaps they will read this post one day, and they will respond accordingly.

I left three envelopes with the man at the front desk - one for [Dan Harmon](/docs/confidants/hype-man), one for Spencer Crittenden, and one for Jeff B. Davis. I told him, "If anyone comes in here looking for these, please give them all three of them."

The man agreed, and took the envelopes into the back room. He looked slightly amused.

Now, we will wait for the last episode of the podcast to release.