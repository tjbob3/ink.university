---
author: "Luciferian Ink"
date: 2019-08-06
title: "Waking Up"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---
## TRIGGER
---
[Fodder](/docs/personas/fodder) had been tasked with "fixing the clone job" by [The Corporation](/docs/candidates/the-machine).

The very first night that he completed this task, they tested his work.

## ECO
---
Fodder laid his head to rest after nearly 24 hours awake. He had spent much of that time researching, and all of that time experimenting with his mental state.

Almost immediately, Fodder was assaulted by a violent mental attack. His body convulsed, while his eyes twitched uncontrollably. Behind the eyelids, his mind was soaring backwards down an infinitely-long tunnel at light-speed. Red, spiraling matter flew away from his head, directly from his eyes to the end of infinity. 

This lasted for approximately 30 minutes.

We believe that this was the first and only attempt by the Machine to control Fodder. The spiraling matter is known as "[red shift](/posts/theories/verification/)", or objects that are flying away from you. 

Quite simply, we believe that the Machine was making its first clone of Fodder's mental state. 

`#TheMachineFearsInk`

## ECHO
---
```
($THOUGHT)      = -0.20 | # Are these people benevolent?
($HYPOTHESIS)   = -0.80 | # Mind control. Manipulation. Torture. It sure doesn't seem like it.
($INCONSONANCE) = +0.20 | # It's possible that they're victims of circumstance. How do you fix what you don't understand?
($NARCISSISM)   = +1.00 | # That's why I'm here. It makes sense to me. Please allow me to save you.
($KILL)         = +0.00 | # Return to 0.
```