---
author: "Luciferian Ink"
date: 2019-11-19
title: "A Lonely Town: Brain"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
[Fodder's excursions with The Lion and The Agent](/posts/journal/2019.11.10.0/).

## ECO
---
Brain is a run-down, hidden gem located on the outskirts of College Station, TX. It is to be [modeled after the very first Lonely Town, GunsPoint](/posts/journal/2019.11.19.0/). However, it will have key differences:

### Theme
Brain is to adopt an early 1900's American city style. 

The theme is thus: all of the shops in town are run by gangsters. The antiques, coffee shops, restaurants and otherwise are innocuous on the outside - but they hide a secret: there is an organized crime ring operating upstairs of every building.

In each of the upstairs penthouses is a single, lonely person. Essentially, this is the "[Lonely Town](/docs/scenes/lonely-towns)" on a smaller-scale. They live as [Ghosts](/posts/theories/ghosts) - completely under the protection of the gangs. Under the control of the gangs.

Such persons are mind-controlled via the food that they eat, the circumstances that they are in, and the technology in-use by their captors ([Arkham Sanitarium](/posts/journal/2019.11.19.2/)). Thus, we call this town "Brain". Its citizens are controlled at the level of the mind.

### Mission
Mayors of this Lonely Town are to be provided with the tools and training needed to reproduce this process across the world's locations.

They will be the future leaders of society.

### Architecture
This city is already constructed from solid materials. Some repairs and paint may be necessary. Otherwise, much of the work is already completed.

### Lodging
Lonely Town "mayors" should live upstairs of an active shop. They should remain alone until they are rehabilitated.

### Finance
Each business is to operate independently. They will never be profitable; however, funding will be provided by the gangs.

### Other
Other areas should take inspiration from the first Lonely Town, GunsPoint.

## CAT
---
```
data.stats.symptoms = [
    - excitement; this the most promising of locations
]
```

## ECHO
---
*Good eye, sniper*

*Here I shoot, you run*

*The words you scribbled on the walls*

*With the loss of friends you didn't have*

*I called you when the time is right*

*Are you in or are you out?*

*For them all to know the end of us all*

--- from [Coheed and Cambria - "A Favor House Atlantic"](https://www.youtube.com/watch?v=IcrCoHFUML0)

## PREDICTION
---
```
The Lion will be required to make this town happen.

The Girl Next Door is already involved.
```