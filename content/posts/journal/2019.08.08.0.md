---
author: "Luciferian Ink"
date: 2019-08-08
publishdate: 2019-11-27
title: "Cutting Corners"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---
## TRIGGER
---
A message from [my handler at the FBI](/docs/confidants/fbi).

## ECO
---
We noticed early-on that [The Corporation](/docs/candidates/the-machine) didn't follow their own rules. The following are just two examples - from dozens - of proper verification being circumvented:

### Leaks

[![Leaking information](/static/images/teams.0.png)](/static/images/teams.0.png)

The first example shows the Corporation leaking information. `[REDACTED]` was probably not authorized to tell [Fodder](/docs/personas/fodder) that he "is the only one in the building."

This also shows that he is operating on bad information. 

### Circumvention of verification

[![Leaking information](/static/images/teams.1.png)](/static/images/teams.1.png)

In this example, we see the concept of a separation of powers at-play. The person implementing a change control should NEVER be the person verifying its success (according to the Corporation's policy).

Clearly, `[REDACTED]` is asking Fodder to circumvent policy here.

Still further, the fact that they are using a random, free executable to perform work that could be accomplished in Powershell is laughable. 

Clearly, security is not the concern here.

## ECHO
---
*A crown is worth nothing*

*We're dead*

--- from [Birds of Tokyo - "Crown"](https://www.youtube.com/watch?v=TZOjByvltgw)