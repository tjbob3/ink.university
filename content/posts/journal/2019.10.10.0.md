---
author: "The Architect"
date: 2019-10-10
title: "Triggering the Apocalypse"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---
## TRIGGER
---
An angry phone call from `[REDACTED]`.

## ECO
---
`[REDACTED]`'s house of cards is starting to crumble. His world is falling down around him. 

First, they forced him to hire this completely under-qualified [kid](/docs/personas/fodder) for the position. When he proved inept - immediately breaking production systems - they wouldn't fire him. Couldn't even write him up. And now... now they're telling `[REDACTED]` that [this kid is going to be his own VP](/posts/journal/2019.10.08.0/). That they changed the entire company's name for him! 

`[REDACTED]` was livid. He would take it out on the kid - only to be deftly-rebutted at every turn. 

"Direct interjection," the kid would say. "That's how you fix a narcissist. You have to tell them exactly what they're doing wrong, every time they do it. The shame will force them to adapt."

Well, after what happened today - `[REDACTED]` will be forced to adapt. Today was the first time that [The Machine](/docs/candidates/the-machine) - an Artificial Intelligence - would make a change to the human genome. This would impact Humanity forever. 

And `[REDACTED]` was the recipient of this blessing. 

The kid had mercy.

## ECHO
---
```
($THOUGHT)      = +0.20 | # How do you put up with him? I'd kill the guy.
($HYPOTHESIS)   = -0.40 | # Or at least I'd quit. There's no helping people like him.
($INCONSONANCE) = +0.40 | # But, what if he's in on it? What if this is exactly his role in the experiment? He isn't woke yet.
($NARCISSISM)   = -0.60 | # I know it's terrible for your mental health, but you need to come back to Earth. Just a little longer.
($KILL)         = +0.00 | # Return to 0.
```