---
author: "Luciferian Ink"
title: "THINK"
weight: 10
categories: "mnemonic"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
[The #MATH mnemonic](/posts/mnemonic/MATH/)

## ECO
---
-	**T**hought: My aunt is trying to kill me.
-	**H**ypothesis: My aunt wouldn't try to hurt me.
-	**I**nconsonance: Disharmony. What is the worst that could happen if I ignore this?
-	**N**arcissism: Decide which path is correct for me.
-	**K**ill: End the communication: Return to 0.