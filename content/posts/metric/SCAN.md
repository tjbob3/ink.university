---
author: "Luciferian Ink"
title: "SCAN Rank"
weight: 10
categories: "metric"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
A change control.

## ECO
---
The SCAN rank is a matrix, formatted as such:
```
2 | 3
1 | 4
```
1. Simplicity
2. Complexity
3. Ambiguous
4. Not-known