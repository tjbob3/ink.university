# Drafts
## Questions
---
Unanswered questions related to unexplained events.

- [Uluru](/posts/questions/u/)
- [Themata](/posts/questions/themata/)
- [Shutterspeed](/posts/questions/shutterspeed/)
- [Roquefort](/posts/questions/slavery/)
- [Fear](/posts/questions/fear/)
- [Center of the Earth](/posts/questions/center-of-the-earth/)
- [Answers](/posts/questions/answers/)

## Hypotheses
---
Early theories that lack supporting evidence.

  - [Anonymity](/posts/hypotheses/anonymity/)
  - [Automation](/posts/hypotheses/automation/)
  - [Code Paradigms](/posts/hypotheses/code/)
  - [Conspiracy Theories](/posts/hypotheses/conspiracies/)
  - [Evolution](/posts/hypotheses/evolution/)
  - [Flat Earth](/posts/hypotheses/flat-earth/)
  - [Fodder is Sold Into Slavery](/posts/hypotheses/hell/)
  - [Game Theory](/posts/hypotheses/game-theory/)
  - [Gender](/posts/hypotheses/gender/)
  - [Genesis](/posts/hypotheses/genesis/)
  - [Immutability](/posts/hypotheses/immutability/)
  - [Mind](/posts/hypotheses/mind/)
  - [Physics](/posts/hypotheses/physics/)
  - [Sound](/posts/hypotheses/sound/)
  - [Symbolism](/posts/hypotheses/symbolism/)
  - [The End](/posts/hypotheses/the-end/)
  - [Toxicity](/posts/hypotheses/toxicity/)
  - [Trial in Absentia](/posts/hypotheses/law/)

## Tests
---
### Standard
- [Test #0: Compress the Dictionary](/posts/tests/test.0)
- [Test #1: Revise the Past](/posts/tests/test.1)

### A/B
- [Test #2: Angelica/Anjelica](/posts/tests/test.2)
- [Test #3: Mike/Mikael](/posts/tests/test.3)

### A/B/C
- [Test #4: Davos/Davis/Jeff](/posts/tests/test.4)
- [Test #5: Influence the Future](/posts/tests/test.5)
- [Test #6: Three Identical Strangers](/posts/tests/test.6)

## Lessons
---
Life lessons by Ink himself.

- [0 - Language Matters](/posts/lessons/1/)

## Mnemonics
---
Memory tools that will help you better process information.

- [FERAL](/posts/mnemonic/FERAL/)
- [MATH](/posts/mnemonic/MATH/)
- [MEGA](/posts/mnemonic/MEGA/)
- [OCEAN](/posts/mnemonic/OCEAN/)
- [THINK](/posts/mnemonic/THINK/)

## Bans
---
Forbidden items.

- Jul 1, 2019
  - [Identity](/posts/bans/ban.0/)
- Aug 1, 2019
  - [Tobacco](/posts/bans/ban.1/)
- Sep 1, 2019
  - [Coffee](/posts/bans/ban.2/)
- Oct 1, 2019
  - [Internet](/posts/bans/ban.3/)
- Nov 1, 2019
  - [Work](/posts/bans/ban.4/)
- Dec 1, 2019
  - [Voice](/posts/bans/ban.5/)
- Jan 1, 2020
  - [Freedom](/posts/bans/ban.6/)
- Feb 1, 2020
  - [Happiness](/posts/bans/ban.7/)
- Mar 1, 2020
  - [Music](/posts/bans/ban.8/)
- Apr 1, 2020
  - [Fractions](/posts/bans/ban.9/)
- May 1, 2020
  - [Money](/posts/bans/ban.10/)
- Jun 1, 2020
  - [Computer Programming](/posts/bans/ban.11/)

## Prophecy
---
Predictions about the future.

- [The Garden Time Forgot](/posts/prophecy/2019.11.25.0/)
- [The Day that the World Breaks Down](/posts/prophecy/2019.11.13.0/)
- [So Above, So Below](/posts/prophecy/2019.10.20.0/)