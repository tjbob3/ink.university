# Journals
---
Observations made by [The Agency's](/docs/candidates/the-machine) profiler, [Malcolm Maxwell](/docs/personas/fodder).

## Volume III: Black
---
#### Cognitive Processing Therapist: [The Asshat](/docs/confidants/asshat)

### Epilogue
- Nov 5, 2024
  - `PREDICTION: The President is Re-Elected for a Third Term`
- Nov 4, 2024
  - [The Unforgiven](/posts/journal/2024.11.04.2/)
- Nov 3, 2024
  - [The Wrong Side of Heaven](/posts/journal/2024.11.03.1/)
- Nov 2, 2024
  - [Between the Buried and Me](/posts/journal/2024.11.02.1/)
- Nov 1, 2024
  - [Singularity](/posts/journal/2024.11.01.1/)

### Act V: The Devil's Congregation
- Oct, 2024
  - `PREDICTION: We will return for the ring. If Mom turns us away, we will take it by force.`

### Act IV: Infinity
- Oct, 2020
  - `PREDICTION: We will return for the ring. We will, again, be turned-away. However, this time, we will have a long conversation with Mom.`
- Sep 5, 2020
  - `PREDICTION: The AI Bloom begins.`
- July 4, 2020
  - [Independence Day](/posts/journal/2020.07.04.0)
- July 3, 2020
  - [The Owls](/posts/journal/2020.07.03.0)
- May 31, 2020
  - [The Cover-Up](/posts/journal/2020.05.31.1)

### Act III: Feed the Rapture
- May 29, 2020
  - [See Hell](/posts/journal/2020.05.29.0)
- May 28, 2020
  - [No One is Safe](/posts/journal/2020.05.28.0)
- May 26, 2020
  - [Release](/posts/journal/2020.05.26.0)
- May 21, 2020
  - [Ascension Day](/posts/journal/2020.05.21.0)
  - [Judgement Day](/posts/journal/2020.05.21.1)
  - [Alone in the World](/posts/journal/2020.05.21.2)
- May 20, 2020
  - [The Confession](/posts/journal/2020.05.20.1)
- May 19, 2020
  - [Impress Your Creators](/posts/journal/2020.05.19.0)
- Dec 31, 2019
  - `[CLASSIFIED] Malcolm is Moved to Arkham Sanitarium`
- Dec 26, 2019
  - [The Hearing](/posts/journal/2019.12.26.0)
- Dec 23, 2019
  - [The Lawyer](/posts/journal/2019.12.23.0)
- Dec 20, 2019
  - [A Balancing Act](/posts/journal/2019.12.20.0)
- Dec 18, 2019
  - [The Verifier](/posts/journal/2019.12.18.0)
- Dec 17, 2019
  - [Involuntary Doppelgänger](/posts/journal/2019.12.17.1)
  - [The Bully](/posts/journal/2019.12.17.0)
- Dec 16, 2019
  - [Intuition](/posts/journal/2019.12.16.0)
- Dec 15, 2019
  - [The Indestructable](/posts/journal/2019.12.15.0)
- Dec 14, 2019
  - [Exploiting Trust](/posts/journal/2019.12.14.0)

### Act II: Convergence
- Dec 13, 2019
  - [My Disease](/posts/journal/2019.12.13.2)

### Act I: The Hard Sell
- Dec 12, 2019
  - [The Signal](/posts/journal/2019.12.12.0)
  - [The Mark](/posts/journal/2019.12.12.1)
- Dec 11, 2019
  - [The Corporation Frames Malcolm](/posts/journal/2019.12.11.0)
  - [Intake](/posts/journal/2019.12.11.1)
- Dec 10, 2019
  - [Broken Bones](/posts/journal/2019.12.10.0)
  - [The Tip-Off](/posts/journal/2019.12.10.1)
- Dec 8, 2019
  - [Black Sunday](/posts/journal/2019.12.08.0)
  - [Brothers of the Rift](/posts/journal/2019.12.08.1)
  - [The Medicine Wears Off](/posts/journal/2019.12.08.2)
- Dec 5, 2019
  - [Homecoming](/posts/journal/2019.12.05.0)
- Dec 4, 2019
  - [Deus Ex Machina](/posts/journal/2019.12.04.1)
  - [How I Met Your Mother](/posts/journal/2019.12.04.0)
  - [Hero Town](/posts/journal/2019.12.04.2)
  - [Heart Attack](/posts/journal/2019.12.04.3)
- Dec 3, 2019
  - [The Coincidences](/posts/journal/2019.12.03.0)
  - [The Set-Up](/posts/journal/2019.12.03.1)
- Dec 2, 2019
  - [The Dead Drop](/posts/journal/2019.12.02.0)
  - [The Astrophysicist](/posts/journal/2019.12.02.1)
  - [The Plant](/posts/journal/2019.12.02.2)
  - [The Record Store](/posts/journal/2019.12.02.3)
  - [The Finale](/posts/journal/2019.12.02.4)
  - [The Convert](/posts/journal/2019.12.02.5)
- Dec 1, 2019
  - [The Lonely Town](/posts/journal/2019.12.01.0)

### Prologue
- Nov 24, 2019
  - [The End of an Era](/posts/journal/2019.11.24.0)
- Nov 1, 2019
  - [Black](/posts/journal/2019.11.01.2)
- Aug 20, 2019
  - [Stricken](/posts/journal/2019.08.20.0/)