# Lonely Towns
## RECORD
---
```
Name: Lonely Towns
Alias: ['$ANYTHING', 'Colonies']
Classification: Sanctuary
Variables:
  $NET_POSITIVE: +1.00 | # Invaluable work. Education. Housing. Food. Contact.
  $COMMUNAL:     +0.78 | # Large and open shared spaces.
  $SAFE:         +0.55 | # Generally safe. Early settlers may struggle for a few years.
```
## ECO
---
Repurposed, refurbished communal living spaces. Typically, large malls or shopping complexes.

The average room is small, while far more space is given to shared living quarters. The self-contained ecosystem has no dependencies; it is self-staffed, self-sustaining (food and energy), self-governing, and human-centered. 

Spread throughout the halls are 1-person pop-up shops, filled with trinkets, refreshments, games, and other events. Shops would open and close at all times of the day. Shops would change from day to day. There would be celebration days, orchestrated events, and mild element of role-playing. The people here would live happily and fulfilled. 

The name "Lonely Town" comes from how they are created. Lonely Towns are created by a single person, whose life purpose was to perform verification of a scientific test. Once completed, they are to be joined with their perfect match - and rule the community together.

## PREDICTION
---
```
The Surgeon needs to invest in the wild pork industry.
```