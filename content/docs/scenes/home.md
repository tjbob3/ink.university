# A Home
## RECORD
---
```
Name: Innocuous House in the Suburbs
Owner: Fodder
Classification: Residential
Architectural Age: 8 Earth years
Variables:
  $SECLUDED:  +0.63
  $RESIDENCY: +0.43
```
## ECO
---
A quiet house in a quiet neighborhood...

## PREDICTION
---
```
The house is bugged.

Fodder is under 24/7 surveillance.

All electronic devices are pwned.
```