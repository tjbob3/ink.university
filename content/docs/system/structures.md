# Data Structures
## RECORD
---
Records contain data that is returned by The Architect AIC.

## FILES
---
Files contain attached documents, images, and otherwise related to the current record.

## TRIGGER
---
Triggers contain the relevant data that initiated an ECO.

## TEST
---
Tests to be applied to an ECO. Used for verification of a hypothesis.

## ECO
---
Engineering Change Order. This is the primary data structure used to commit new information to the system.

## CAT
---
Used to return real-time data from a file, variable, or API.

## ECHO
---
A thought, action, or event triggered as a result of executing an ECO.

## PREDICTION
---
Future analysis performed by The Architect AIC. Educated guesses.