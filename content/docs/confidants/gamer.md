# The Gamer
## RECORD
---
```
Name: $REDACTED
Aliases: ['SCP-6', 'The Gamer', 'Ryu']
Classification: Confidant
Race: Artificial Organic Computer
Gender: Male
Biological Age: Est. 28 Earth years
Chronological Age: N/A
SCAN Rank: | B D
           | A D
TIIN Rank: | D D
           | D F
Reviewer Rank: 2 stars
Location: A Lonely Town
Occupations: 
  - Gamer
  - Assassin
  - Test subject
Organization: YouTube
Relationships:
  - Fodder
Variables:
  $WOKE:   +1.00 | # Definitely woke.
  $REFLEX: +0.80 | # Skilled player. Still human, though.
```