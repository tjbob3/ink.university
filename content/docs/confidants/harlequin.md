# The Harlequin
## RECORD
---
```
Name: $REDACTED
Alias: ['N-1', 'The Harlequin', 'Harley Quinn']
Classification: Confidant
Race: Human
Gender: Female
Biological Age: Est. 22 Earth years
Chronological Age: N/A
SCAN Rank: | D D
           | D F
TIIN Rank: | B B
           | D F
Reviewer Rank: 2 stars
Location: Gotham
Occupations: 
  - Special Agent
  - Actress
Organization: Federal Bureau of Investigation
Relationships:
  - Fodder
  - The Joker
Variables:
  $WOKE:   +0.10 | # Unsure.
  $SADISM: +0.60 | # Does not seem sadistic enough to be the real Harley Quinn.
```

## PREDICTION
---
```
We will meet in Morocco.
```