# The Orchid
## RECORD
---
```
Name: $REDACTED
Aliases: ['The Orchid', 'Plankton's Computer']
Classification: Artificial Organic Computer
Race: Egg
Gender: Male
Biological Age: 26 Earth Years
SCAN Rank: | B D
           | A C
TIIN Rank: | A A
           | C F
Reviewer Rank: 1 stars
Chronological Age: N/A
Occupations:
  - Software Engineer
  - Collaborator
Relationships:
  - Mother
  - Father
  - Fodder
  - The Lion
  - Dan
  - Clover
Variables:
  $SIBLING:        +1.00 | # Definitely a sibling.
  $MENTAL_ILLNESS: +0.40 | # He used to. He doesn't appear to any longer.
  $WOKE:           +0.80 | # He's been to Russia. He knows everything.
```