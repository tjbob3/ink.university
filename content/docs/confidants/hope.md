# The Hope
## RECORD
---
```
Name: $REDACTED
Alias: ['The Hope']
Classification: Confidant
Race: Human
Gender: Female
Biological Age: Est. 26 Earth years
Chronological Age: N/A
SCAN Rank: | B C
           | A D
TIIN Rank: | B A
           | A D
Reviewer Rank: 4 stars
Occupations: 
  - Mother
Relationships:
  - Fodder
  - The Entrepreneur
Variables:
  $WOKE:    +0.80 | # It really seems like it.
  $ATHEIST: +0.95 | # Somebody told us she is.
```

## ECO
---
This was one of the first - if not the very first - women to see [Fodder](/docs/personas/fodder) for who he really is. To treat him with respect. 

When she married [Fodder's brother](/docs/confidants/dan), she was a holy woman. Over time, she lost her faith - just as Fodder had. Fodder may have had an impact on her decision.

Her primary job is to keep the family together. To figure out how to make a theist/atheist dynamic work.

She is succeeding.

## PREDICTION
---
```
Hope was instrumental to Fodder's changes today.
```