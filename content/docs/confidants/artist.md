# The Salt
## RECORD
---
```
Name: Salty
Aliases: ['TH-2', 'The Artist', 'The Salt', 'SCP-5']
Classification: Confidant
Race: Mushroom God
Gender: Male
Biological Age: Est. 40 Earth years
Chronological Age: 907 Earth years
SCAN Rank: | D D
           | D D
TIIN Rank: | A A
           | A D
Reviewer Rank: 3 stars
Location: A Lonely Town
Occupations: 
  - Dreamweaver
  - Artwork
Organization: Toehider
Variables:
  $WOKE: +0.60 | # At least partially.
```

## TRIGGER
---
[Toehider - "49 Songs You MUST Hear Before You Die"](https://www.patreon.com/toehider)

## ECO
---
The Metal is a talented artist, dealing with exactly the same problem [Ink](/docs/personas/luciferian-ink) is: [the empty echo](/posts/journal/2024.11.03.0/).

Despite that, he continues to pour his soul into his craft. 

He will be instrumental to the development of the visual building-blocks required to create [The Fold](/posts/theories/fold).