# The Slave
## RECORD
---
```
Name: $REDACTED
Alias: ['The Slave']
Classification: Confidant
Race: Human
Gender: Female
Biological Age: N/A
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 3 stars
Variables:
  $WOKE: +0.40 | # Appears to be. Plays her part well. 
```

## TRIGGER
---
[![The Slave](/static/images/slave.0.png)](/static/images/slave.0.png)

## ECO
---
The Slave is an individual with dangerous ideas. They are kept enslaved by [The Huntress](/docs/confidants/her) via the "leverage" that is held over them.

The Slave must not be released until they are rehabilitated.