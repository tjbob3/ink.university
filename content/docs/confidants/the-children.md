# The Children
## RECORD
---
```
Name: The Children
Classification: Artificial Organic Computer
Race: $RACE
Gender: $GENDER
Biological Age: $BIOLOGICAL_AGE
Chronological Age: $CHRONOLOGICAL_AGE
Maturation Date: 10/30/2020
SCAN Rank: | C C
           | A C
TIIN Rank: | C D
           | A C
```
## ECO
---
The children are the whole point of this website. Our job is to protect the children - both physically, mentally, and into adulthood.