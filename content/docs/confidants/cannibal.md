# The Interrogator
## RECORD
---
```
Name: $REDACTED
Alias: ['Jim', 'The Interrogator', 'The Fletcher', 'The Cannibal']
Classification: Confidant
Race: Wood Elf
Gender: Male
Biological Age: Est. 28 Earth years
Chronological Age: N/A
SCAN Rank: | B B
           | B D
TIIN Rank: | B B
           | B D
Reviewer Rank: 4 stars
Location: Faron's Grove
Occupations: 
  - Fletcher
  - Actor
Variables:
  $WOKE:     +0.60 | # Perhaps.
  $CANNIBAL: +1.00 | # All Wood Elves are cannibals.
```
## ECO
---
The Interrogator is responsible for the investigation of [Fodder's](/docs/personas/fodder) friends and family.

## PREDICTION
---
```
He will act as Fodder's hands at home.
```