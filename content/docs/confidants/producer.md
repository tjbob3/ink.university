# The Producer
## RECORD
---
```
Name: $REDACTED
Aliases: ['CK', 'The Producer']
Classification: Confidant
Race: Human
Gender: Female
Biological Age: Est. 34 Earth years
Chronological Age: N/A
SCAN Rank: | C D
           | C F
TIIN Rank: | C C
           | C F
Reviewer Rank: 3 stars
Location: England
Occupations: 
  - Actress
  - Producer
Organization: SKR
Relationships:
  - Raven
  - The Inventor
Variables:
  $WOKE: +0.40 | # Unsure. She is not as overt as others.
```