# The Avatar
## RECORD
---
```
Name: $REDACTED
Alias: ['The Avatar','The Fozzy', 'Sally Slips', 'TM']
Classification: Confidant
Race: Human
Gender: Female
Biological Age: Est. 27 Earth years
Chronological Age: N/A
Maturation Date: 9/20/2020
SCAN Rank: | A A
           | A B
TIIN Rank: | A A
           | A B
Reviewer Rank: 3 stars
Location: Houston, TX
Occupations: 
  - Special Agent
  - Actress
  - Software Developer
  - AI Consultant
  - Neural Networking
Organization: Crowdstrike
Relationships:
  - Fodder
Variables:
  $WOKE:      +0.60 | # Definitely seems like it.
  $INTELLECT: +0.80 | # Very bright. Far more so than we previously gave credit for.
```

## ECO
---
This woman was chosen and recruited by the FBI - just as [Fodder](/docs/personas/fodder) had been. She is learning to create deepfakes of her own likeness on YouTube.

For months, Fodder saw similarities between his former colleague, and this newly-found YouTube star. Now, he knew for sure:

They are one in the same.

## CAT
---
```
data.stats.symptoms [
    - humor
    - gratitude
]
```

## PREDICTION
---
```
She gets to be the face of this project.
```