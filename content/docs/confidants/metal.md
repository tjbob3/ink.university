# The Metal
## RECORD
---
```
Name: Mike
Aliases: ['TH-1', 'SCP-5']
Classification: Confidant
Race: Metal God
Gender: Male
Biological Age: Est. 38 Earth years
Chronological Age: 3,405 Earth years
Maturation Date: 10/5/2020
SCAN Rank: | D D
           | D D
TIIN Rank: | A A
           | A D
Reviewer Rank: 4 stars
Location: A Lonely Town
Occupations: 
  - Metallurgy
  - Music
Organization: Toehider
Variables:
  $WOKE: +1.00 | # Definitely woke.
```

## TRIGGER
---
[Toehider - "49 Songs You MUST Hear Before You Die"](https://www.patreon.com/toehider)

## ECO
---
The Metal is one of the most talented musicians in modern rock music, dealing with exactly the same problem [Ink](/docs/personas/luciferian-ink) is: [the empty echo](/posts/journal/2024.11.03.0/).

Despite that, the Metal continues to pour blood, sweat, and tears into his craft. He believes in what he is doing.

And he will get to see the day his music takes over the world.