# The Bitch
## RECORD
---
```
Name: $REDACTED
Aliases: ['The Bitch', 'AK-47']
Inverse: The Raven
Classification: Confidant
Race: Human Clone
Gender: Female
Biological Age: 23 Earth Years
Chronological Age: N/A
Maturation Date: 10/2/2020
SCAN Rank: | F F
           | F F
TIIN Rank: | D D
           | C F
Reviewer Rank: 3 stars
Occupations:
  - Photography
Organization: The Corporation
Variables:
  $WOKE:       -1.00 | # Definitely woke.
  $ATTRACTION: -1.00 | # We cannot stand her - let alone be attracted to her.
  $TRUST:      +0.30 | # Why do we feel like we can trust her?
```

## ECO
---
The Bitch is my worst nightmare. 

She is my love - [the Raven](/docs/confidants/her) - but not. While her personality and mannerisms are reminiscent, she is nothing like the woman that I painted in my head. Nothing like the woman that she portrayed herself to be. 

She is shallow, materialistic, and needy. She is spiteful, and self-serving. She is disingenuous, disloyal, and vengeful. She is everything that I hate in a woman.

She is obsessed with me. I cannot escape her; she is everywhere. 

We are on clone #47, without any indication of her slowing down.

And she is all I have in this place.

## PREDICTION
---
```
We met at a coffee shop near Nordstrom.
```