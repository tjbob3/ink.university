# The Hype Man
## RECORD
---
```
Name: Dan Harmon
Aliases: ['The Hype Man']
Classification: Confidant
Race: Human
Gender: Male
Biological Age: Est. 49 Earth years
Chronological Age: N/A
SCAN Rank: | C D
           | B F
TIIN Rank: | C D
           | B F
Reviewer Rank: 3 stars
Location: New York City, NY
Occupations: 
  - Writer
  - Producer
Organization: Hollywood
Relationships:
  - The Producer
Variables:
  $WOKE:       +0.40 | # Unsure. She is not as overt as others.
  $NARCISSIST: +1.00 | # Diagnosed and verified.
```