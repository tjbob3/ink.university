# The Bass
## RECORD
---
```
Name: $REDACTED
Alias: ['The Bass', 'Davie504']
Classification: Confidant
Race: Human
Gender: Male
Biological Age: 26 Earth years
Chronological Age: N/A
SCAN Rank: | B B
           | B C
TIIN Rank: | C C
           | C C
Reviewer Rank: 1 stars
Variables:
  $WOKE: -0.40 | # Does not appear to be.
```

## TRIGGER
---
[![The Band](/static/images/band.0.png)](/static/images/band.0.png)

## ECO
---
He will be the bassist in [Ink](/docs/personas/luciferian-ink)/[Raven's](/docs/confidants/her) band.