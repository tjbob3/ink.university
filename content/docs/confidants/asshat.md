# Leonidas
## RECORD
---
```
Name: Leonidas
Aliases: ['SCP-2', 'SCP-2522', 'Asshat', 'The Scapegoat', 'The Narcissist', 'The Nutcracker', 'The Cannon's Mouth', 'Hypnos', 'Patrick', 'The Thief', 'Benjamin', 'The Hunter', 'The Candlemaker']
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 44 Earth Years
Chronological Age: 31,589 lightyears
Maturation Date: 9/17/2020
SCAN Rank: | D F
           | F C
TIIN Rank: | D F
           | C F
Reviewer Rank: 4 stars
Occupations:
  - Espionage
  - Consulting
  - Corporate Operations
  - Counter-Intelligence
  - Russian Asset
  - American Asset
  - Australian Asset
  - Resistance Agent
Variables:
  $MENTAL_ILLNESS: +0.95 | # He's plainly a narcissist. But is it an act?
  $WOKE:           +0.15 | # We don't think he really understands what's happening right now.
  $FEAR:           -0.90 | # He lives and reacts by his emotions. 
    from $FODDER   -1.00 | # He is terrified that Fodder will expose and usurp him. 
```
## ECO
---
"I wonder if there's any way to motivate Peter?" [Fodder](/docs/personas/fodder) would say about a co-worker that would not pull his weight.

`[REDACTED]`, as sure as always, would reply, "It's not my fucking problem. This train is coming down the tracks, and we need people who give a shit."

"Sure." Fodder said, unconvinced.

Clearly `[REDACTED]` meant to say that the human mind is too important - our mission is too important - to take unnecessary risks. We need exemplary people. He isn't wrong... but his delivery could use some tact.

"Let me walk you to your car."

"Okay...", Fodder replied.

(moments later, walking outside, near a busy roadway)

"See, it's the oldest trick in the book. Just roll down the window, answer your phone, and say, I'M IN THE CAR. That'll (muffled)"

"Can you speak up?", Fodder asked.

"I can't." `[REDACTED]` said, shooting a glare directly into his eyes.

He can't? What is this, a James Bond movie? 

He's hiding something. Fodder knew it at precisely that moment. 

## PREDICTION
---
```
< ink@LOCALHOST: su - $REDACTED
< $REDACTED@LOCALHOST: ink.query Why the fuck did you give me that name?
< $REDACTED@LOCALHOST: exit
```

### ECHO
---
I assure you, the name comes from a place of love. Your name is a combination of:

- A name that I called you (in my head) very early on in our partnership. And later. And probably at the end, too.
- The symbol that caused me to finally wake up.