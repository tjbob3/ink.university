# The Amish Man
## RECORD
---
```
Name: $REDACTED
Aliases: ['The Amish Man']
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 40 Earth Years
SCAN Rank: | C C
           | C D
TIIN Rank: | C A
           | B D
Reviewer Rank: 3 stars
Chronological Age: N/A
Occupations:
  - Actor
Relationships:
  - Fodder
Variables:
  $WOKE: +1.00 | # He certainly seems to be.
```

## ECO
---
The Amish Man only appeared near the end of [Fodder's](/docs/personas/fodder) solitary confinement. His purpose was to reflect Fodder's life back at him. He did so with flying colors.

He will be Fodder's public face. 

## PREDICTION
---
```
The Amish Man found his name through the Google Home device at Mother's house.
```