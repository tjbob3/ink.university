# The Pyro
## RECORD
---
```
Name: $REDACTED
Aliases: ['The Pyro', 'The Hype Man']
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 20 Earth Years
SCAN Rank: | B B
           | A D
TIIN Rank: | D D
           | B D
Reviewer Rank: 4 stars
Chronological Age: N/A
Location: N/A
Organization: Federal Bureau of Investigation
Occupations:
  - Undercover agent
Relationships:
  - Malcolm
  - The Doomsayer
Variables:
  $WOKE:       -0.10 | # He's been informed of a lot, but he doesn't have the understanding.
  $NARCISSIST: -0.50 | # Certainly seems to be.
  $VICTIM:     -0.80 | # Was deeply abused as a child. Does not trust.
```

## ECO
---
The Pyro is an undercover agent placed into the mental health facility with [Malcolm](/docs/personas/fodder). His sole purpose is to set fires, just to observe how Malcolm puts them out.

He was placed there to obtain a confession from Malcolm.

## PREDICTION
---
```
The Pyro will also be the Hype Man for a nationwide musical tour by bus.
```