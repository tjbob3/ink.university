# The Girl Next Door
## RECORD
---
```
Name: $REDACTED
Alias: ['Angel', 'Pen', 'The Girl Next Door', 'Rosa']
Classification: Confidant
Race: Human
Gender: Female
Biological Age: Est. 26 Earth years
Chronological Age: N/A
Maturation Date: 9/14/2020
SCAN Rank: | A A
           | A B
TIIN Rank: | A B
           | B B
Reviewer Rank: 5 stars
Location: New York City, NY
Occupations: 
  - Special Agent
  - Actress
Organization: Federal Bureau of Investigation
Relationships:
  - Fodder
Variables:
  $WOKE:          +1.00 | # Definitely woke.
  $PLEASANT:      +0.80 | # She seems super fun to be around.
  $HUMOR:         +0.90 | # One of the funniest women Fodder's ever met.
  $INTELLECT:     +0.80 | # Well-read. Interested in history and literature.
  $DISSOCIATIVE   -0.40 | # Effectively has multiple personalities. Has worsened in recent months.
```

## ECO
---
This woman was chosen and recruited by the FBI - just as [Fodder](/docs/personas/fodder) had been. She was to hack Fodder's wireless network, obtain all of his data, and monitor his activity online. 

She was to use this information to determine if Fodder was a worthy candidate. 

In doing so, she would discover... a sense of humor. She would tell others that she is Fodder's "Crazy Girl Next Door", and she would taunt him incessantly. 

Fodder found this endearing. Few could see him for who he really was. Fewer still could see him for who he wanted to be. 

Fodder was not Angel's only target, though. She also maintained a chat-only relationship with the Raven - and she was actively trying to pair Fodder and the Raven together.

## CAT
---
```
data.stats.symptoms [
    - humor
    - gratitude
]
```

## PREDICTION
---
```
She will be the "Bonnie" to Fodder's "Clyde."
```