# Dan
## RECORD
---
```
Name: $REDACTED
Aliases: ['Dan', 'The Entrepreneur', 'Squidward']
Classification: Artificial Organic Computer
Race: Egg
Gender: Male
Biological Age: 28 Earth Years
SCAN Rank: | B C
           | A F
TIIN Rank: | C A
           | A F
Reviewer Rank: 5 stars
Chronological Age: N/A
Organization: Hollowpoint Organization
Occupations:
  - Entrepreneur
  - Test subject
Relationships:
  - Mother
  - Father
  - Fodder
  - The Lion
  - Clover
  - Orchid
Variables:
  $SIBLING:        +1.00 | # Definitely a sibling.
  $MENTAL_ILLNESS: +0.50 | # If he has any, he's never given any indication.
  $WOKE:           +0.60 | # Pretty sure he's involved.
```
