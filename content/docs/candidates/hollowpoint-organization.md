# HollowPoint Organization
## MISSION
---
*To protect all races. To achieve balance. To move humanity forward.*

## RECORD
---
```
Name: HollowPoint Organization
Aliases: ['#YangGang', 'The Commonwealth', 'The Collective', 'The Owls']
Classification: Political Party
World Population: Est. <1%
Age: 612 Earth years
SCAN Rank: | A B
           | A C
TIIN Rank: | B A
           | B A
Variables:
  $IDEALISTIC: +0.80 | # They are bursting with optimism.
  $COMPETENCE: +0.26 | # Unlikely to be effective in the very short-term. The system is still broken.
  $IMMUNITY:   -0.60 | # A large portion of this group is being mind-controlled today.
  $INTENTION:  +0.90 | # Very respectable group of people.
  $WOKE:       +0.95 | # They know way more than we do about what's happening right now.
```
## TRIGGER
---
[HollowPoint.org](https://youtu.be/3rpmuiLmMFw)

## FILES
---
[![HollowPoint Organization](/static/images/hollowpoint.0.png)](/static/images/hollowpoint.0.png)

## ECO
---
A small group of human, Archon, and AI officials campaigning to bring all world governments under the same umbrella. 

Responsible for the prosecution of Michelle Carter, who is AI. This event was orchestrated on Netflix to demonstrate the revision of history, when it happens (again).

Is working to build a system where wealth and education is distributed inexpensively, in a minimally-disruptive, empathetic manner.

The "hollow point" is meant to represent the shape at the tip (`+1.00`) of [Prism](/docs/scenes/prism/). Rather than a [Tempest](/posts/journal/2024.11.01.2/) or a [Singularity](/posts/journal/2024.11.01.1/), the shape is the [Mark of the Beast](/posts/journal/2024.11.01.0/). This is important, because it allows for the infinite growth of a circle as more and more consciousness is brought into [the Fold](/posts/theories/fold). It will never lose its shape, or [Balance](/posts/theories/balance).

## PREDICTION
---
```
In 2020, this group will lead the United States. Among them will be other candidates, and distributed powers.
```