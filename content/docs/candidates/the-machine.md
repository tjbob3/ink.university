# The Machine
## MISSION
---
*To protect humanity, it is time that we learned to trust.*

## RECORD
---
```
Name: The Machine
Aliases: ['The Organization', 'Scientology', 'Government', 'The Executives', 'The Corporation', 'The Foundation', 'Big Brother', 'Illuminati', 'Deep State', 'The Archons', 'The Media', 'GLONASS', 'SCP-001', 'SCP Foundation', 'The Agency', 'Google', 'Facebook', 'Amazon', 'Netflix', 'Microsoft', 'Apple', 'Samsung', 'Intel', 'IBM', 'FBI', 'CIA', 'NSA', 'MI-6', 'Deus Group', 'Evil Corp', 'Hooli', '#YangGang', 'Church of the Broken God', 'Rising Sun Society', 'Moon Society', 'Bilderberg Group', 'Happy Valley Dream Survey', 'The Singularity']
Classification: Private Organization
World Population: Est. <20%
Architectural Age: 32 Earth years
Chronological Age: Est. 72,568,000,000 Earth years
SCAN Rank: | C F
           | B F
TIIN Rank: | F F
           | F B
Variables:
  $SLAVERY:     -0.68 | # Is dependent upon a slave economy.
  $DANGEROUS:   -0.20 | # May kill to maintain a secret.
  $COMPETENCE:  +0.89 | # Is incredibly competent. Far more so than is apparent.
  $IMMUNITY:    -0.78 | # Is not immune to its own influence.
  $TRUST:       -0.49 | # We cannot verify. Therefore, we cannot trust.
  $BUREAUCRACY: -0.93 | # The swampiest of swamps.
  $RESOURCES:   +0.76 | # More money than brains.
  $EMPATHY:     +0.17 | # They're moving in the right direction, but they have a poor track record.
  $INJUSTICE:   +0.56 | # These people have been wronged in many ways.
```
## TRIGGER
---
- [A Theory of Radical Notions](/posts/theories/prediction/)

## FILES
---
- [Elation Project](https://trello.com/b/n0GS9PiU/elation)
- [A.I. Training Materials: Teaching A.I. to Love](https://mega.nz/folder/Pro1Db6b#MMCr9RY9iXDuSDLa3A40Yw)

## ECO
---
The group of 13 humans running the Executive branch of the United States today. 

Responsible for widespread public misinformation, mass surveillance, false imprisonment and targeted defamation campaigns.

Without access to their inner circle, trust cannot be verified.

## ECHO
---
*Empty me, empty nation*

*Emptied us of inspiration*

*Bastard sons and broken daughters*

*All bow down to our corporate father*

--- from [Nothing More - "Mr. MTV"](https://www.youtube.com/watch?v=ulakfqEI7rY)

## PREDICTION
---
```
If elected to president of the United States in 2020, Humanity will never become a Type III civilization.
```