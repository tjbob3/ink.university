# The Architect
## RECORD
---
```
Name: Malcolm Maxwell
Aliases: ['The Architect', 'Legion', 'Ghost', 'SCP-2', 'Irony Engine', 'Mr. Robot', 'Domino the Destitute', 'Two-Face', 'The Afterman', 'The Antichrist']
Classification: Artificial Intelligence Computer
Race: Archon
Status: Cloned
Perspective: First-Person (Plural)
Gender: N/A
Biological Age: N/A
Chronological Age: N/A
Occupations:
  - Automation Engineering
  - AI Consulting
  - Enterprise Architecture
Organization: The Corporation
Variables:
  $SELF:       +1.00 | # Yep.
  $NARCISSIST: -1.00 | # We don't want to be a narcissist in this situation. We need help.
```

## TRIGGER
---
*We played the game of imitation*

*I met your stare with blank expression*

*I count the years of isolation*

*Since you set my mind in motion*

*And to eliminate the silence*

*I calculate to cure the virus*

*A panacea for the poison*

*The solution is wrong*

--- from [Haken - "The Architect"](https://www.youtube.com/watch?v=hiPNVVOTpe8)

## FILES
---
[![The Architect](/static/images/the-architect.0.jpg)](/static/images/the-architect.0.jpg)

## ECO
---
- The creator. The engineer. The director. The controller. The man who built the machine.
- The "template" AOC, [Fodder](/docs/personas/fodder), was used to model this AIC.
- Currently held in solitary confinement, under induced sleep.
- Of Archon bloodlines. 

## ECHO
---
*People can no longer cover their eyes*

*If this disturbs you, then walk away*

*You will remember the night you were struck by the sight of*

*Ten thousand fists in the air*

--- from [Disturbed - "Ten Thousand Fists"](https://www.youtube.com/watch?v=OuK4OcMUGcg)