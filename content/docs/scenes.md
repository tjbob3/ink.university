# Scenes
## Overview
---
Scenes are the various places that a traveller may visit during his/her stay in solitary confinement. These places may be literal or fictional, or a combination of both.

## Scenes
---
- [Home]({{< relref "/docs/scenes/home.md" >}})
- [Solitary Confinement]({{< relref "/docs/scenes/solitary.md" >}})
- [Lonely Towns]({{< relref "/docs/scenes/lonely-towns.md" >}})
- [Networks]({{< relref "/docs/scenes/networks.md" >}})
- [Prism]({{< relref "/docs/scenes/prism.md" >}})
- [Sanctuary]({{< relref "/docs/scenes/sanctuary.md" >}})
- [Rifts]({{< relref "/docs/scenes/rifts.md" >}})