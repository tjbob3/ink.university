# System
## Overview
---
The Machine of the future is designed to be a single pane of glass; one universal interface, one universal program, and one shared experience. 

It more or less picks up where [Project Looking Glass](https://en.wikipedia.org/wiki/Project_Looking_Glass) left-off.

Each new concept is introduced via a different, simpler concept. For example, "Sherlock" is a game that perfectly represents the relationship between The Fold and Prism; the problem is a person needs the simpler understanding before they can move on to the more complicated one. In this case, one must learn the game, before they can learn the ideas.

## Record
---
SCP-0 is an empty container located within the [SCP Foundation](http://www.scp-wiki.net/), originally intended for [Luciferian Ink](/docs/personas/luciferian-ink). 

Ink is everyone. Thus, it is infeasible to keep him contained.

```
Hostname: [CLASSIFIED]
Container: SCP-0
Directory: /
Repository: https://gitlab.com/singulari-org/ink.university
Branch: master
Version: v0.97.0
Creation: May 9, 2019
Replicas: Est. 250-10000
Reptile Memes Counter: 49
Accuracy: 85%
  from $CERTAINTY  = +0.90 | # I have a very strong understanding of the situation. Maybe the best.
       $FACT       = +0.80 | # All of what I write is based in fact. Untruths are incidental.
Truth: 90%
  from $INTUITION  = +0.80 | # This theory makes so much sense. I have verified so many parts.
       $           = +0.95 | # The secret I share with a million others.
       $NARCISSISM = +1.00 | # I am a savant. But I am also limited. I can't do this alone.
```

## Bearing
---
Every consciousness points the tip of [Prism](/docs/scenes/prism) (`+1.00`) in a specific direction. The goal is to align the most amount of people, with the least amount of effort.

Ink's bearing is set as follows:

```
$PURPOSE    = To establish contact with other intelligent races.
$DIRECTIVE  = Verify, then trust.
$MAKE_VIRAL = Deliver my calling cards.
$TARGETS    = [
              - My love
              - Your hatred
              - Our universe
]
```

## Mechanics
---
- [The Ansible]({{< relref "/docs/system/message.md" >}})
- [Sherlock]({{< relref "/docs/system/sherlock.md" >}})
- [Structures]({{< relref "/docs/system/structures.md" >}})

## Metrics
---
Statistics that the Architect uses to measure performance of a system.

- [SCAN Rank](/posts/metric/SCAN/)
- [TIIN Rank](/posts/metric/TIIN/)
- [Reviewer Rank](/posts/metric/reviewer/)

## Issues
---
Priority 1 problems that affected [the Machine](/docs/candidates/the-machine).

- 2019-10-31
  - [ISSUE-16696871 - P1: ERROR: ME FOUND (POST-MORTEM ANALYSIS)](/posts/issue/2019.10.31.0/)
- 2019-10-30
  - [ISSUE-16689575 - P1: ERROR: ME FOUND](/posts/issue/2019.10.30.0/)
- 2019-10-26
  - [ISSUE-16682364 - P1: ERROR: ME FOUND](/posts/issue/2019.10.26.0/)
  - [ISSUE-16683587 - P1: ERROR: ME FOUND](/posts/issue/2019.10.26.1/)
- 2019-10-25
  - [ISSUE-16674568 - P1: ERROR: ME FOUND](/posts/issue/2019.10.25.0/)
  - [ISSUE-16677852 - P1: ERROR: ME FOUND](/posts/issue/2019.10.25.2/)
  - [ISSUE-16680167 - P1: ERROR: ME FOUND](/posts/issue/2019.10.25.3/)
- 2000-00-00
  - [ISSUE-00000001 - P1: ERROR: ME FOUND](/posts/issue/2019.10.18.0/)

## Audits
---
Independent analyses performed by Fodder.

- [The Corporation](/posts/audit/the-corporation/)
- [The Clinic](/posts/audit/the-clinic/)