# The Pillars

## Overview
---
Pillars are the physical and mental structures that support an individual's identity. Buildings. Objects. Concepts. Matter. These structures are used to prop-up the individual's definition of truth, whatever that may be. In the same way that steel beams hold towers together, mental Pillars hold an individual's identity together.

The Pillars give us a starting-place; essentially, it places a person into a specific location on a map, at a specific point-in-time. From there, the person's consciousness may fork and travel as-necessary down alternate paths, as described by [The Fold](/posts/theories/fold).

Importantly, Pillars are considered to be immutable; unchanging. Once set, they are very difficult to unset. It is for this reason that it is imperative we begin to change immediately. The process is slow. We must teach children, the mentally-ill, the incarcerated, the violent, and more immediately. This theory will help them reach enlightenment. It will not happen overnight.

## Pillars
---

### Birth
We are but sparks, born into a random point-in-time, and traveling the down same paths as our predecessors.

### Anonymity
My name is Legion, and we are many.

### Connection
The exchange of ideas and experience between identities.

### Verification
`Verify, then trust.`

### Health
Mental and physical.

### Education
Education should be engaging. You should want to go to school.

### Ethics
Everyone deserves a second chance. Even you.

### Art
Because context matters.

### Spirituality
This is the feeling you get while reaching forward, molding the future.

### Science
Because true things are testable. 

### Technology
`Engineering. Automation. Artificial Intelligence.`

### Mind
Become one with [The Fold](/posts/theories/fold).

### Order
There are just some things that the world cannot compromise on.

### Anarchy
`Randomness generates new experiences.`

### Defense
`End world conflict. Focus on protecting the human mind.`

### Narcissism
Fall in love with your own mind.

### Reproduction
Sex. Cloning. Genetics. Procedural generation.

### Relaxation
We deserve a stress-free existence.

### Experience
Teach others to reach a fulfilled life.

### Gratitude
Build a Monument to those you love. To those who shaped you - good or bad.

### Optimism
*It's just your condition*

*To question it all, to be opposition*

*I've done the math, I've worked it all out*

*Everything functions despite all this doubt*

--- from [Tub Ring - "Optimistic"](https://www.youtube.com/watch?v=2n1ymVci8VI)

### Intention
Focus attention on tasks that benefit the most amount of people.

### Agreement
Find common ground with everyone you meet.

### Sanctuary
Return home.

